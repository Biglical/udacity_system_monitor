#ifndef PROCESS_H
#define PROCESS_H

#include "processor.h"
#include <string>
#include <vector>
/*
Basic class for Process representation
It contains relevant attributes as shown below
*/
class Process {
public:
  Process(int);
  int Pid();                              // TODO: See src/process.cpp
  std::string User();                     // TODO: See src/process.cpp
  std::string Command();                  // TODO: See src/process.cpp
  float CpuUtilization();                 // TODO: See src/process.cpp
  std::string Ram();                      // TODO: See src/process.cpp
  long UpTime();                          // TODO: See src/process.cpp
  bool operator<(Process const &a) const; // TODO: See src/process.cpp
  void Update();

  std::string user_;
  float cpu_;
  std::string ram_;
  long up_time_;
  std::string command_;
  // TODO: Declare any necessary private members
private:
  int pid_;
  Processor pidProc;
};

#endif