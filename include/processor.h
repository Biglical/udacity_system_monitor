#ifndef PROCESSOR_H
#define PROCESSOR_H

class Processor {
public:
  float Utilization(); // TODO: See src/processor.cpp
  float Utilization(int pid);
  // TODO: Declare any necessary private members
private:
  long long last_cpu{0};
  long long last_idle{0};
  long long proc_last_seconds{0};
  long long proc_last_total_time{0};
};

#endif