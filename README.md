# CppND-System-Monitor


## What I did
The project was an ncursers shell of a program that needed all of its backend data to be displayed to be dug out of the various files in `/proc/`. The project has 3 classes that need to be mangaged as well as a namspace `LinuxParser`, that is used to parse all linux files needed

![System Monitor](images/monitor.png)



## ncurses
[ncurses](https://www.gnu.org/software/ncurses/) is a library that facilitates text-based graphical output in the terminal. This project relies on ncurses for display output.

If you are not using the Workspace, install ncurses within your own Linux environment: `sudo apt install libncurses5-dev libncursesw5-dev`

## Make
This project uses [Make](https://www.gnu.org/software/make/). The Makefile has four targets:
* `build` compiles the source code and generates an executable
* `format` applies [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) to style the source code
* `debug` compiles the source code and generates an executable, including debugging symbols
* `clean` deletes the `build/` directory, including all of the build artifacts

