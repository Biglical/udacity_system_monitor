#include <cctype>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include "linux_parser.h"
#include "process.h"
#include "processor.h"

using std::string;
using std::to_string;
using std::vector;

// TODO: Return this process's ID
int Process::Pid() { return pid_; }

// TODO: Return this process's CPU utilization
float Process::CpuUtilization() { return pidProc.Utilization(pid_); }

// TODO: Return the command that generated this process
string Process::Command() { return LinuxParser::Command(pid_); }

// TODO: Return this process's memory utilization
string Process::Ram() { return LinuxParser::Ram(pid_); }

// TODO: Return the user (name) that generated this process
string Process::User() { return LinuxParser::User(pid_); }

// TODO: Return the age of this process (in seconds)
long Process::UpTime() { return LinuxParser::UpTime(pid_); }

// TODO: Overload the "less than" comparison operator for Process objects
// REMOVE: [[maybe_unused]] once you define the function
void Process::Update() {
  user_ = Process::User();
  cpu_ = Process::CpuUtilization();
  ram_ = Process::Ram();
  up_time_ = Process::UpTime();
  command_ = Process::Command();
}
bool Process::operator<(Process const &a) const { return a.cpu_ > cpu_; }
Process::Process(int pid) : pid_(pid) {
  user_ = Process::User();
  cpu_ = Process::CpuUtilization();
  ram_ = Process::Ram();
  up_time_ = Process::UpTime();
  command_ = Process::Command();
  Processor pidProc;
}
