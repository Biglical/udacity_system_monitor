#include "processor.h"
#include "linux_parser.h"
#include <iostream>
#include <math.h>
#include <string>
#include <unistd.h>
#include <vector>

// TODO: Return the aggregate CPU utilization
float Processor::Utilization() {
  std::vector<std::string> cpu_data = LinuxParser::CpuUtilization();
  long long total_cpu = 0;
  for (std::string cpu_type : cpu_data) {
    total_cpu += stof(cpu_type);
  }
  long long current_cpu = total_cpu;
  long long current_idle = stof(cpu_data[LinuxParser::CPUStates::kIdle_]);
  double return_value = (1.0 - ((double)(current_idle - last_idle) /
                                (double)(current_cpu - last_cpu)));
  last_cpu = current_cpu;
  last_idle = current_idle;
  return (float)return_value;
}

float Processor::Utilization(int pid) {
  std::vector<std::string> items = LinuxParser::CpuUtilization(pid);
  if (items.size() >= LinuxParser::ProcStates::starttime_) {
    long total_time = std::stol(items[LinuxParser::ProcStates::utime_]) +
                      std::stol(items[LinuxParser::ProcStates::stime_]) +
                      std::stol(items[LinuxParser::ProcStates::cutime_]) +
                      std::stol(items[LinuxParser::ProcStates::cstime_]);
    long time_started_after_boot =
        (std::stol(items[LinuxParser::ProcStates::starttime_]) /
         sysconf(_SC_CLK_TCK)) /
        1000;
    long seconds = LinuxParser::UpTime() - time_started_after_boot;
    float return_value =
        (float)((total_time / (double)sysconf(_SC_CLK_TCK)) / seconds);
    return return_value;
  }
  return 0.0;
}