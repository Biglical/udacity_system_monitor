#include "linux_parser.h"

#include <dirent.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using std::stof;
using std::string;
using std::to_string;
using std::vector;

// DONE: An example of how to read data from the filesystem
string LinuxParser::OperatingSystem() {
  string line;
  string key;
  string value;
  std::ifstream filestream(kOSPath);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ' ', '_');
      std::replace(line.begin(), line.end(), '=', ' ');
      std::replace(line.begin(), line.end(), '"', ' ');
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == LinuxParser::koperatingSystemToken) {
          std::replace(value.begin(), value.end(), '_', ' ');
          return value;
        }
      }
    }
  }
  filestream.close();
  return string();
}

// DONE: An example of how to read data from the filesystem
string LinuxParser::Kernel() {
  string os, kernel, version;
  string line;
  std::ifstream stream(kProcDirectory + kVersionFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> os >> version >> kernel;
    stream.close();
  }
  return kernel;
}

vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR *directory = opendir(kProcDirectory.c_str());
  struct dirent *file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}

// DONE: Read and return the system memory utilization
float LinuxParser::MemoryUtilization() {
  string mem_total;
  string mem_available;
  string scratch;
  string line;
  std::ifstream stream(kProcDirectory + kMeminfoFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> scratch >> mem_total;
    std::getline(stream, line);
    std::getline(stream, line);
    std::istringstream linestream2(line);
    linestream2 >> scratch >> mem_available;
    stream.close();
  }
  return 1 - ((float)std::stof(mem_available) / std::stof(mem_total));
}

// DONE: Read and return the system uptime
long LinuxParser::UpTime() {
  string uptime;
  string line;
  std::ifstream stream(kProcDirectory + kUptimeFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> uptime;
    stream.close();
    return stoi(uptime);
  }
  return 0;
}

vector<string> LinuxParser::CpuUtilization() {
  vector<string> items{};
  string line;
  string name;
  std::ifstream stream(kProcDirectory + kStatFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    // Pop the CPU name off the string
    linestream >> name;
    while (linestream >> name) {
      items.push_back(name);
    }
    stream.close();
  }
  return items;
}

// DONE: Read and return CPU utilization
vector<string> LinuxParser::CpuUtilization(int pid) {
  vector<string> items{};
  string line;
  string name;
  std::ifstream stream(kProcDirectory + to_string(pid) + kStatFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    // Pop the CPU name off the string
    linestream >> name;
    while (linestream >> name) {
      items.push_back(name);
    }
    stream.close();
  }
  return items;
}
// DONE: Read and return the total number of processes
int LinuxParser::TotalProcesses() {
  string line;
  string value;
  string token;
  std::ifstream stream(kProcDirectory + kStatFilename);
  if (stream.is_open()) {
    while (std::getline(stream, line)) {
      std::istringstream linestream(line);
      while (linestream >> token >> value) {
        if (token == LinuxParser::kprocessString) {
          stream.close();
          return stoi(value);
        }
      }
    }
  }
  stream.close();
  return 0;
}

// DONE: Read and return the number of running processes
int LinuxParser::RunningProcesses() { return LinuxParser::Pids().size(); }

// DONE: Read and return the command associated with a process
string LinuxParser::Command(int pid) {
  string line;
  string command;
  std::ifstream stream(kProcDirectory + to_string(pid) + kCmdlineFilename);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    linestream >> command;
    stream.close();
    return command;
  }
  return string();
}
// DONE: Read and return the memory used by a process
string LinuxParser::Ram(int pid) {
  string line;
  string token;
  string value;
  char mb[100];
  std::ifstream stream(kProcDirectory + to_string(pid) + kStatusFilename);
  if (stream.is_open()) {
    while (std::getline(stream, line)) {
      std::istringstream linestream(line);
      while (linestream >> token >> value) {
        // VmRSS instead of VmSize
        if (token == LinuxParser::kVmRSSString) {
          // 1024 is because VmRSS is in kb
          sprintf(mb, "%.3f", stof(value) / 1024.0);
          return string(mb);
          stream.close();
        }
      }
    }
  }
  return string();
}

// DONE: Read and return the user ID associated with a process
string LinuxParser::Uid(int pid) {
  string line;
  string token;
  string value;
  std::ifstream stream(kProcDirectory + to_string(pid) + kStatusFilename);
  if (stream.is_open()) {
    while (std::getline(stream, line)) {
      std::istringstream linestream(line);
      while (linestream >> token >> value) {
        if (token == LinuxParser::kUIDString) {
          stream.close();
          return value;
        }
      }
    }
  }
  return string();
}

// DONE: Read and return the user associated with a process
string LinuxParser::User(int pid) {
  string line;
  string token_uid;
  string place_holder;
  string name;
  string uid = LinuxParser::Uid(pid);
  std::ifstream stream(kPasswordPath);
  if (stream.is_open()) {
    while (std::getline(stream, line)) {
      std::replace(line.begin(), line.end(), ':', ' ');
      std::istringstream linestream(line);
      while (linestream >> name >> place_holder >> token_uid) {
        if (token_uid == uid) {
          stream.close();
          return name;
        }
      }
    }
  }
  return string();
}

// DONE: Read and return the uptime of a process
long LinuxParser::UpTime(int pid) {
  string line;
  string name;
  std::ifstream stream(kProcDirectory + to_string(pid) + kStatFilename);
  vector<string> items{};
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    // Pop the CPU name off the string
    while (linestream >> name) {
      items.push_back(name);
    }
  };
  if (items.size() >= LinuxParser::ProcStates::starttime_) {
    // added LinuxParser::UpTime() to acurally calculate uptime per code review
    stream.close();
    return (long)(LinuxParser::UpTime() - (double)std::stol(items[starttime_]) /
                                              (double)sysconf(_SC_CLK_TCK));
  }
  return 0;
}
