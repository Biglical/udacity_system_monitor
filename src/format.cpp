#include <stdio.h>

#include <string>

#define SECONDS_IN_MINUTE 60
#define MINUTES_IN_HOUR 60

#include "format.h"

using std::string;

// TODO: Complete this helper function
// INPUT: Long int measuring seconds
// OUTPUT: HH:MM:SS
// REMOVE: [[maybe_unused]] once you define the function

string Format::ElapsedTime(long seconds) {
  // Iterger devide should nock off remaining seconds
  int hours = seconds / (SECONDS_IN_MINUTE * MINUTES_IN_HOUR);

  // Convert to minutes, modules by minutes in and hour will be remaining
  // minutes not used up in the hour
  int minutes = (seconds / SECONDS_IN_MINUTE) % MINUTES_IN_HOUR;

  int remaining_seconds = seconds % SECONDS_IN_MINUTE;

  char c_string_time[100];
  sprintf(c_string_time, "%02d:%02d:%02d", hours, minutes, remaining_seconds);
  string string_time = string(c_string_time);

  return string_time;
}